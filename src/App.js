import './App.css';
import Navbar from './components/Navbar';
import TextForm from './components/TextForm';
function App() {
  return (
    <>
   
<Navbar title="Enigmatix" aboutText="About"/>
<div className="container">
<TextForm heading="Enter the Text Below"/>
</div>
    </>
  );
}

export default App;
